{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedRecordDot #-}

module Foo where

import Bar
import Control.Lens
import Data.Generics.Labels ()
import GHC.Records


barRecord :: Bar
barRecord = Bar
  { x = 1 -- This shows a warning
  , y = 'a'
  }

barFieldAccessor :: Bar -> Int
barFieldAccessor = x -- This shows a warning

barDot :: Bar -> Int
barDot b = b.x -- This does not

barOverloadedLabel :: Bar -> Int
barOverloadedLabel = view #x -- This does not

barGetField :: Bar -> Int
barGetField = getField @"x" -- This does not
