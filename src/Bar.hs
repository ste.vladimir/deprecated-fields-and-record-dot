{-# LANGUAGE DeriveGeneric #-}

module Bar where

import GHC.Generics


data Bar = Bar
  { x :: Int
  , y :: Char
  } deriving (Generic)

{-# DEPRECATED x "Don't use me" #-}
